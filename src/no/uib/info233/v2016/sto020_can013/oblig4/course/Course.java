
package no.uib.info233.v2016.sto020_can013.oblig4.course;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import no.uib.info233.v2016.sto020_can013.oblig4.utils.Utils;

import java.util.HashMap;

/**
 * @author sto020
 */
public class Course {


    private String course_code; // String for the course code
    private String name = null; // name of the course
    private int leaderID; // lecturer of the course
    private String season = null;
    private String exam_type = null;
    private String finalGrade = null;
    private BooleanProperty enroll = new SimpleBooleanProperty(false);
    private HashMap<Integer,Part> courseParts = new HashMap<>(); // hashmap for all the courseParts, i.e group assignments etc.

    public BooleanProperty enrollProperty() {
        return enroll;
    }

    /**
     * Constructor
     * @param course_code the input to be set to course code field.
     * @param name the input name of the course
     * @param leaderID the input to be the lecturer of the course
     * @param season the input to be the season of the course
     * @param exam_type the input to be the type of exam for the course.
     */
    public Course(String course_code, String name, int leaderID, String season, String exam_type) {
        this.course_code = course_code; // sets value of field variables to parameter variables.
        this.name = name;
        this.leaderID = leaderID;
        this.season = season;
        this.exam_type = exam_type;
    }

    /**
     * Generates a custom String
     * @return The generated string
     */
    @Override
    public String toString() {
        return String.format(" %s: %s", this.course_code, this.name);
    }

    /**
     * Calculates the final grade based on the weight and grade of all Course Parts.
     * If any of the Course parts are missing a grade, it will not set a final Grade.
     *
     * @return the calculated final grade
     */
    public String calculateFinalGrade() {
        double finalGrade = 0.0;
        for (Part part : getCourseParts().values()) {
            if(part.getGrade() ==  null || part.getGrade().isEmpty()) return getFinalGrade();
            double tmpGrade = (part.getWeight() / 100) * Utils.convertLetterGradeToNumber(
                    part.getGrade());

            finalGrade += tmpGrade;
        }
        setFinalGrade(Utils.convertNumberGradeToLetter(finalGrade));
        return getFinalGrade();
    }

    /**
     * Method for adding a part to the course.
     * @param part the input object value to be added to the course.
     */
    public void addCoursePart(Part part) {
        courseParts.put(part.getID(),part);
    }

    /**
     * Set method
     * @param part the course part to be updated.
     */
    public void updateCoursePartGrade(Part part) {
        courseParts.get(part.getID()).setGrade(part.getGrade());
    }

    /**
     * Get method
     * @return leaderID the leader for the course.
     */
    public int getLeaderID() {
        return leaderID;
    }

    /**
     * Set method
     * @param leaderID the value to be set for the field variable.
     */
    public void setLeaderID(int leaderID) {
        this.leaderID = leaderID;
    }

    /**
     * Get method
     * @return season the season of the course.
     */
    public String getSeason() {
        return season;
    }

    /**
     * Set method
     * @param season the input value to be set to the new field variable value of season.
     */
    public void setSeason(String season) {
        this.season = season;
    }

    /**
     * Get method
     * @return exam_type the type of exam to be weighted.
     */
    public String getExam_type() {
        return exam_type;
    }

    /**
     * Set method
     * @param exam_type the new value for field variable exam_type
     */
    public void setExam_type(String exam_type) {
        this.exam_type = exam_type;
    }

    /**
     * Get method
     * @return name the name of the course.
     */
    public String getName() {
        return name;
    }

    /**
     * Set method
     * @param name the new value for field variable name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get method
     * @return the courseParts the hashmap for course parts.
     */
    public HashMap<Integer, Part> getCourseParts() {
        return courseParts;
    }

    /**
     * Set method
     * @param courseParts the courseParts to set
     */
    public void setCourseParts(HashMap<Integer, Part> courseParts) {
        this.courseParts = courseParts;
    }

    /**
     * Get method
     * @return course_code the code for the course.
     */
    public String getCourse_code() {
        return course_code;
    }

    /**
     * Set method
     * @param course_code the new value for field variable course_code.
     */
    public void setCourse_code(String course_code) {
        this.course_code = course_code;
    }

    /**
     * Get method
     * @return finalGrade the final grade for the course.
     */
    public String getFinalGrade() {
        return finalGrade;
    }

    /**
     * Set method
     * @param finalGrade the new value for field variable finalGrade.
     */
    public void setFinalGrade(String finalGrade) {
        this.finalGrade = finalGrade;
    }

}
