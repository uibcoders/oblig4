/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig4.course;

/**
 * Represents a Part of a Course
 * @author sto020
 *
 */
public class Part {


	private int ID;



	private int group_id;
	private String name; // The name of the graded assignment.
	private String grade;
	private double weight; // The weight of the part grade.

	/**
	 * Constructor for Part class
	 * @param name the name to be set to the Part.
	 * @param weight the weight to be set to the Part.
     */
	public Part(String name,double weight)
	{
		this.setName(name);
		this.setWeight(weight);
			}

	/**
	 * Generates a custom String
	 * @return The generated string
	 */
	@Override
	public String toString() {
		return String.format("%s : Grading: %s %% Grade: %s", this.name, this.weight,this.grade);
	}

	/**
	 * Get method
	 * @return ID the ID for the course part.
     */
	public int getID() {
		return ID;
	}

	/**
	 * Set method
	 * @param ID the new value to be set to field variable ID.
     */
	public void setID(int ID) {
		this.ID = ID;
	}

	/**
	 * Get method
	 * @return The group id or 0
	 */
	public int getGroup_id() {
		return group_id;
	}

	/**
	 * Set method
	 * @param group_id The group id
	 */
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	/**
	 * get method
	 * @return name the name of the course part.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set method
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get method
	 * @return weight the weight of the course part.
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * Set method
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		if (checkWeight(weight)){this.weight = weight;}
	}

	/**
	 * Method for checking if the weight is equal or above 20 and equal or less than 100.
	 * @param weight the weight to be checked.
	 * @return weight the weight if the check accepts it.
     */
	private boolean checkWeight(double weight)
	{
		return weight >= 20 && weight <= 100;
	}

	/**
	 * Get method
	 * @return grade the grade for the course part.
     */
	public String getGrade() {
		return grade;
	}

	/**
	 * Set method
	 * @param grade the new value to be set to field variable grade.
     */
	public void setGrade(String grade) {
		this.grade = grade;
	}
}
