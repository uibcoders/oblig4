package no.uib.info233.v2016.sto020_can013.oblig4.utils;

/**
 * Created by sto020 on 08.05.2016.
 */
public class Utils {


    /**
     * Converts a letter grade between A-F to a number grade between 0.0-5.0
     * @param grade The grade to convert to number
     * @return The converted grade/Number
     */
    public static double convertLetterGradeToNumber(String grade) {
        double numberGrade = 0.0;//Default

        if (grade == null || !grade.matches("[A-Fa-f]")) // check if grade wasn't specified, or
        {// if grade is not between A to F upper or lowercase
            System.out.println("Invalid letter grade");
            return numberGrade;
        }

        if (grade.equalsIgnoreCase("A")) {
            numberGrade = 5.0;
        } else if (grade.equalsIgnoreCase("B")) {
            numberGrade = 4.0;
        } else if (grade.equalsIgnoreCase("C")) {
            numberGrade = 3.0;
        } else if (grade.equalsIgnoreCase("D")) {
            numberGrade = 2.0;
        } else if (grade.equalsIgnoreCase("E")) {
            numberGrade = 1.0;
        } else if (grade.equalsIgnoreCase("F")) {
            numberGrade = 0.0;
        }

        return numberGrade;
    }


    /**
     * Converts a n umber grade between 0.0-5.0 to a letter grade between A-F
     * @param grade
     * @return
     */
    public static String convertNumberGradeToLetter(double grade) {
        String letterGrade = "F"; //Default

        if (grade > 5  || grade < 0 ){
        System.out.println("Invalid number grade");
        return letterGrade;
    }

        int rGrade = (int) Math.round(grade);
        
        if (rGrade == 5) {
            letterGrade = "A";
        } else if (rGrade == 4) {
            letterGrade = "B";
        } else if (rGrade == 3) {
            letterGrade = "C";
        } else if (rGrade == 2) {
            letterGrade = "D";
        } else if (rGrade == 1) {
            letterGrade = "E";
        } else if (rGrade == 0) {
            letterGrade = "F";
        }

        return letterGrade;
    }
}

