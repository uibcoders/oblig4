/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig4.employee;



/**
 * Represents a Student
 * @author sto020
 *
 */
public class Student extends Person {


	/**
	 * @param ID The student ID
	 * @param firstName The student name
	 */
	public Student(int ID, String firstName) {
		super(ID, firstName);
		// TODO Auto-generated constructor stub
	}


}
