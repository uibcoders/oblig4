/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig4.employee;

/**
 * Represent a Person
 * @author sto020
 *
 */
public class Person {
	private int ID;
	private String firstName = null;
	private String lastName = null;


	/**
	 * Constructor
	 */
	public Person(int ID, String firstName) {
		this.ID = ID;
		this.firstName = firstName;
	}

	
	/**
	 * Overrides the toString() to create a custom output string fitted to the
	 * user
	 * 
	 * @return the Custom string
	 */
	@Override
	public String toString() {
		return String.format(" %s: %s ", this.ID, this.firstName);
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}


	/**
	 * Get the first name of the user
	 * 
	 * @return User first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Set the first name of the user
	 * 
	 * @param firstName
	 *            User first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Get the last name of the user
	 * 
	 * @return User last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Set the last name of the user
	 * 
	 * @param lastName
	 *            User last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
