package no.uib.info233.v2016.sto020_can013.oblig4.JUnitTests;

import no.uib.info233.v2016.sto020_can013.oblig4.database.DataSourceQuery;
import org.junit.*;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


/**
 * Created by can013, sto020 on 11.05.2016.
 */
public class SQLConnectionTest {

    DataSourceQuery dsq;

    @Before
    public void setUp() throws Exception {
        dsq = new DataSourceQuery();
    }

    /**
     * Test to check if the return string (Expected result)
     * is equal to the actual result, returns this string if
     * connection successful.
     * Also checks something known to be false.
     */
    @Test
    public void connectionTest() {
        assertEquals("Connected", dsq.connect());
        assertFalse(dsq.connect() != "Connected");
    }

}