package no.uib.info233.v2016.sto020_can013.oblig4.JUnitTests;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import no.uib.info233.v2016.sto020_can013.oblig4.utils.Utils;
/**
 * Created by can013, sto020
 */
public class GradeConversionTest {
    public Utils util;

    @Before
    public void setUp() throws Exception {
        util = new Utils();
    }

    /**
     * Test to check the method for converting
     * grade numbers to grade letters.
     */
    @Test
    public void GradeConversionTest(){
        assertEquals("A", util.convertNumberGradeToLetter(5));
        assertEquals("B", util.convertNumberGradeToLetter(4));
        assertEquals("C", util.convertNumberGradeToLetter(3));
        assertEquals("D", util.convertNumberGradeToLetter(2));
        assertEquals("E", util.convertNumberGradeToLetter(1));
        assertEquals("F", util.convertNumberGradeToLetter(0));
        assertEquals("F", util.convertNumberGradeToLetter(4309));
        assertEquals("F", util.convertNumberGradeToLetter(- 5));

    }

}