package no.uib.info233.v2016.sto020_can013.oblig4.JUnitTests;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import no.uib.info233.v2016.sto020_can013.oblig4.course.Course;
import no.uib.info233.v2016.sto020_can013.oblig4.course.Part;

/**
 * Created by can013, sto020
 *
 */


public class FinalGradeTests {
    Course course;


    @Before
    /**
     * Sets up everything for the test,
     * instantiates classes and gives values to parts
     * and adds them to the course.
     */
    public void setUp() throws Exception {
        Part part1 = new Part("Oblig 1", 20);
        Part part2 = new Part("Oblig 2", 35);
        Part part3 = new Part("Oblig 3", 20);
        Part part4 = new Part("Oblig 4", 25);
        part1.setGrade("A");
        part1.setID(1);
        part2.setGrade("B");
        part2.setID(2);

        part3.setGrade("C");
        part3.setID(3);

        part4.setGrade("A");
        part4.setID(4);
        course = new Course("12345", "INFO233", 55, "Spring", "Digital Exam");

        course.addCoursePart(part1);
        course.addCoursePart(part2);
        course.addCoursePart(part3);
        course.addCoursePart(part4);
    }

    /**
     * AssertTrue to check that the grade calculation is correct.
     * AssertFalse to check that something that is false is correct.
     */
    @Test
    public void FinalGradeTests() {

        assertTrue(course.calculateFinalGrade().equals("B"));
        assertFalse(course.calculateFinalGrade().equals("F"));
        assertFalse(course.calculateFinalGrade().equals("E"));
        assertFalse(course.calculateFinalGrade().equals("C"));
        assertFalse(course.calculateFinalGrade().equals("D"));
        assertFalse(course.calculateFinalGrade().equals("A"));
    }

}