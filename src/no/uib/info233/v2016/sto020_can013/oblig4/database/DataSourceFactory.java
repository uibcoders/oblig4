/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig4.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;


import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

/**
 *
 */
public class DataSourceFactory {

    /**
     *
     * @return the DataSource.
     */
   public static DataSource getMySQLDataSource() {
       Properties props = new Properties();
       FileInputStream fis = null;
       MysqlDataSource mysqlDS = null;
       try {
           fis = new FileInputStream("db.properties");
           props.load(fis);
           mysqlDS = new MysqlDataSource();
           mysqlDS.setURL(props.getProperty("MYSQL_DB_URL"));
           mysqlDS.setUser(props.getProperty("MYSQL_DB_USERNAME"));
           mysqlDS.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));
       } catch (IOException e) {
           e.printStackTrace();
       }
       return mysqlDS;
   }
    

}