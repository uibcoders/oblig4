/**
 *
 */
package no.uib.info233.v2016.sto020_can013.oblig4.database;


import no.uib.info233.v2016.sto020_can013.oblig4.course.Course;
import no.uib.info233.v2016.sto020_can013.oblig4.course.Part;
import no.uib.info233.v2016.sto020_can013.oblig4.employee.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataSourceQuery {

    private List<Student> studentList = new ArrayList<>(); // list of all students
    private List<Part> coursePartsList = new ArrayList<>(); // list of all course parts
    private List<Course> courseList = new ArrayList<>(); // list of all courses
    private DataSource ds; // Datasource field variable.
    private Connection con;
    private boolean connected = false;

    /**
     * Constructor for the class
     * Instantiates field variable ds by calling customized method.
     */
    public DataSourceQuery() {
        ds = DataSourceFactory.getMySQLDataSource();
    }

    /**
     * Connects to the database
     *
     * @return String of connection status, 'Connected' if connected, else an error message.
     */
    public String connect() {
        String connectionString = "Connected";
        try {
            con = ds.getConnection();
        } catch (SQLException e) {
            if (e.getErrorCode() == 1045 || e.getErrorCode() == 1044) {

                connectionString = "Access Denied! Please check credentials and Database name";

            } else if (e.getErrorCode() == 0) {
                connectionString = "Could not connect to database, please check the database URL. Make sure you are connected to the internet!";
            }
            System.out.println(connectionString);

            connected = false;
            return connectionString;
        }
        connected = true;

        return connectionString;
    }

    /**
     * Disconnects from the database
     */
    public void disconnect() {
        try {
            if (con != null) con.close();
        } catch (SQLException e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
            connected = false;
        }
    }

    private void showDisconnected() {
        System.out.println("Client is not connected to database!");
    }

    /**
     * Method for adding students to the database.
     *
     * @param student the student to be added to the database.
     */
    public void addStudent(Student student) {
        if (!connected) {
            showDisconnected();
            return;
        }
        PreparedStatement preparedStmt = null;

        try {

            String query = ("INSERT INTO Students (ID, name)"
                    + " values (?, ?)");

            preparedStmt = con.prepareStatement(query);
            preparedStmt.setInt(1, 0);
            preparedStmt.setString(2, student.getFirstName());

            // execute the preparedstatement
            preparedStmt.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method for updating a grade for a specific students group id
     *
     * @param course_code  the course code
     * @param student_id   the student id
     * @param old_group_id the old group id
     * @param new_group_id the new group id
     */
    public void updateStudentGroupID(String course_code, int student_id, int old_group_id, int new_group_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {

            String query = " UPDATE StudentsCourses SET group_id= ? WHERE course_code= ? AND student_id= ? AND group_id= ?";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setInt(1, new_group_id);
            preparedStmt.setString(2, course_code);
            preparedStmt.setInt(3, student_id);
            preparedStmt.setInt(4, old_group_id);

            preparedStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method for adding courses to the database as well as course parts.
     * For each loop to iterate over hashmap for course parts and uses a SQL query
     * to add it to the database in the CourseParts table.
     *
     * @param course the course to be added to the database.
     */
    public void addCourse(Course course) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {

            String query = ("INSERT INTO Courses (course_code, name, leader_id, season, ExamType)"
                    + " values (?, ?, ?, ?, ?)");

            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, course.getCourse_code());
            preparedStmt.setString(2, course.getName());
            preparedStmt.setInt(3, course.getLeaderID());
            preparedStmt.setString(4, course.getSeason());
            preparedStmt.setString(5, course.getExam_type());
            // execute the preparedstatement
            preparedStmt.execute();

            for (Part part : course.getCourseParts().values()) {
                query = ("INSERT INTO CourseParts (ID, course_code, name,weight)"
                        + " values (?, ?, ?, ?)");
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setString(1, null);
                preparedStmt.setString(2, course.getCourse_code());
                preparedStmt.setString(3, part.getName());
                preparedStmt.setDouble(4, part.getWeight());

                // execute the preparedstatement
                preparedStmt.execute();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method for adding a student to a specific course.
     * uses an INSERT INTO SQL query
     * to add student to a course.
     *
     * @param course_code the course a student would be enrolled in.
     * @param student_id  the student to be enrolled.
     */
    public void enrollStudentToCourse(String course_code, int student_id, int group_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {
            String query = ("INSERT INTO `StudentsCourses` (`course_code`, `student_id` , `group_id`) VALUES (?, ?,?)");
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, course_code);
            preparedStmt.setInt(2, student_id);
            preparedStmt.setInt(3, group_id);

            // execute the preparedstatement
            preparedStmt.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method for updating a grade for a specific student and course part.
     *
     * @param grade      the grade to be updated.
     * @param part_id    the part_id of the grade.
     * @param student_id the student to be graded.
     * @param group_id   the group id the student is assigned
     */
    public void updatePartGrade(String grade, int part_id, int student_id, int group_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {


            if (group_id != 0) {
                String query = " UPDATE CoursePartResults SET grade= ? WHERE id= ? AND group_id= ?";
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setString(1, grade);
                preparedStmt.setInt(2, part_id);
                preparedStmt.setInt(3, student_id);
                preparedStmt.setInt(3, group_id);

            } else {
                String query = " UPDATE CoursePartResults SET grade= ? WHERE id= ? AND student_id= ?";
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setString(1, grade);
                preparedStmt.setInt(2, part_id);
                preparedStmt.setInt(3, student_id);
            }

            preparedStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method for setting Part grades for students in CoursePartResults table.
     *
     * @param course_code the course code to be set.
     * @param grade       the course part grade to be set.
     * @param part_id     the course part id to be set.
     * @param student_id  the student_id to be set.
     */
    public void setPartGrade(String course_code, String grade, int part_id, int student_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {
            String query = "INSERT INTO `CoursePartResults` (`ID`, `course_code`, `student_id`, `grade`,`group_id`) VALUES (?,?,?,?,?);";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setInt(1, part_id);
            preparedStmt.setString(2, course_code);
            preparedStmt.setInt(3, student_id);
            preparedStmt.setString(4, grade);
            preparedStmt.setInt(5, 0);


            preparedStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * Method for setting Part grades for students in CoursePartResults table.
     *
     * @param course_code the course code to be set.
     * @param grade       the course part grade to be set.
     * @param part_id     the course part id to be set.
     * @param student_id  the student_id to be set.
     * @param group_id    the group id the student is assigned
     */
    public void setPartGradeToGroup(String course_code, String grade, int part_id, int student_id, int group_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        //get alll group members

        for (Student s : getStudentsByGroupAndCourse(course_code, group_id)
                ) {


            PreparedStatement preparedStmt = null;

            try {
                String query = "INSERT INTO `CoursePartResults` (`ID`, `course_code`, `student_id`, `grade`,`group_id`) VALUES (?,?,?,?,?);";
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setInt(1, part_id);
                preparedStmt.setString(2, course_code);
                preparedStmt.setInt(3, s.getID());
                preparedStmt.setString(4, grade);
                preparedStmt.setInt(5, group_id);


                preparedStmt.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    preparedStmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Method for setting the final grade to a student at in specific course.
     * uses an INSERT INTO SQL query to insert values in the
     * table rows of CourseResults.
     *
     * @param course_code the course code for the final grade.
     * @param grade       the final grade.
     * @param student_id  the student who has the grade and takes the course.
     */
    public void setCourseGrade(String course_code, String grade, int student_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {
            String query = "INSERT INTO `CourseResults` (`course_code`, `student_id`, `grade`) VALUES (?,?,?);";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, course_code);
            preparedStmt.setInt(2, student_id);
            preparedStmt.setString(3, grade);

            preparedStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method for updating the final grade for a student in a specific course.
     * uses a SQL query to set the grade dependent on
     * a course code and a student ID.
     *
     * @param course_code the course code belonging to the grade.
     * @param grade       the grade to be updated.
     * @param student_id  the student belonging to the grade to be updated.
     */
    public void updateCourseGrade(String course_code, String grade, int student_id) {
        if (!connected) {
            showDisconnected();
            return;
        }

        PreparedStatement preparedStmt = null;

        try {
            String query = " UPDATE CourseResults SET grade= ? WHERE course_code= ? AND student_id= ?";
            preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, grade);
            preparedStmt.setString(2, course_code);
            preparedStmt.setInt(3, student_id);

            preparedStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method that performs a SQL query and joins two tables
     * where the student ID is equal. The joined table is the student's
     * group ID and the grade of all students in that group.
     *
     * @param student_id the student ID to be searched by.
     */
    public void getGroupGradesAndStudents(int student_id) {
        String query = "SELECT StudentsCourses.student_id, StudentGroupGrades.part_grade FROM StudentsCourses, StudentGroupGrades WHERE StudentsCourses.group_id"
                + " = StudentGroupGrades.group_id";
        executeQuery(query);
    }

    /**
     * Method to return a list of student(s) with a specific name.
     *
     * @param name the name to be searched by
     * @return studentList the list of student(s) with a specific name.
     */
    public List<Student> searchStudent(String name) {
        studentList.clear();
        executeQuery("SELECT * FROM Students WHERE name LIKE '%" + name + "%'");
        return studentList;
    }

    /**
     * Method to return a list of all students.
     *
     * @return studentList a list of all students.
     */
    public List<Student> getStudents() {
        studentList.clear();
        executeQuery("SELECT * FROM Students");
        return studentList;
    }

    /**
     * Method to return a list of all students with the group id specified .
     *
     * @param course_code the course id to find
     * @param group_id    the group id to find
     * @return studentList a list of all students with specified parameters.
     */
    public List<Student> getStudentsByGroupAndCourse(String course_code, int group_id) {
        studentList.clear();
        executeQuery("SELECT Students.ID, Students.name FROM Students, StudentsCourses WHERE Students.ID = StudentsCourses.student_id " +
                "AND StudentsCourses.group_id = " + group_id + " AND StudentsCourses.course_code ='" + course_code + "'");
        return studentList;
    }

    /**
     * Method to return a list of information of a specific course.
     *
     * @param course_code the course code to be searched by.
     * @return courseList the information for a specific course.
     */
    public List<Course> getCourse(String course_code) {
        courseList.clear();
        executeQuery("SELECT * FROM Courses WHERE course_code=" + course_code);
        return courseList;
    }

    /**
     * Method to get a list of course parts for a specific course
     * based on input string.
     *
     * @param course_code the course code to be searched by.
     * @return coursePartsList the list of course parts for a course.
     */
    public List<Part> getCourseParts(String course_code) {
        coursePartsList.clear();
        executeQuery("SELECT * FROM CourseParts WHERE course_code='" + course_code + "' ORDER BY name");
        return coursePartsList;
    }

    /**
     * Method to get a list of the different course parts with their respective
     * grades for a student and his/her course.
     *
     * @param course_code the course the student takes.
     * @param student_id  the student.
     * @return coursePartsList the list of part grades for a student in a course.
     */
    public List<Part> getCoursePartsWithGrades(String course_code, int student_id) {
        coursePartsList.clear();
        String query = "SELECT sc.group_id,CoursePartResults.student_id, CourseParts.course_code,CourseParts.name,CourseParts.weight,CourseParts.ID ,CoursePartResults.grade " +
                " FROM StudentsCourses as sc ,CourseParts " +
                " LEFT JOIN CoursePartResults " +
                " ON CourseParts.course_code = CoursePartResults.course_code AND CourseParts.ID = CoursePartResults.ID  and CoursePartResults.student_id = " + student_id +
                " WHERE CourseParts.course_code ='" + course_code + "' AND sc.student_id = " + student_id + "  ORDER BY CourseParts.name";


        executeQuery(query);
        return coursePartsList;
    }

    /**
     * Method to get a list of courses based on a specific student ID
     * uses a SQL query to perform this.
     *
     * @param id the id to be searched by.
     * @return courseList the list of courses with the specified id.
     */
    public List<Course> getCoursesByStudentID(int id) {
        courseList.clear();
        String query = "SELECT sC.course_code,sC.student_id ,c.name,c.leader_id,c.season,c.ExamType, cR.grade " +
                "FROM StudentsCourses sC " +
                "INNER JOIN Courses c ON sC.course_code = c.course_code " +
                "left JOIN CourseResults cR ON c.course_code = cR.course_code AND  cR.student_id = sC.student_id " +
                "WHERE sC.student_id = " + id +
                " ORDER BY sC.course_code";

        executeQuery(query);
        return courseList;

    }

    /**
     * Method that gets all courses through a SQL query.
     *
     * @return courseList the list of all courses.
     */
    public List<Course> getAllCourses() {
        courseList.clear();
        executeQuery("SELECT * FROM Courses");
        return courseList;

    }

    /**
     * Method that takes a SQL query string and executes it based
     * on predetermined if statements.
     *
     * @param query the SQL query to be executed.
     */
    private void executeQuery(String query) {
        if (!connected) {
            showDisconnected();
            return;
        }

        Statement stmt = null;
        ResultSet rs = null;
        System.out.println(query);

        try {
            stmt = con.createStatement();

            rs = stmt.executeQuery(query);

            while (rs.next()) {

                if (query.contains("FROM Students, StudentsCourses") ) {
                    int studID = rs.getInt("ID");
                    String studName = rs.getString("name");

                    studentList.add(new Student(studID, studName));

                } else if (query.contains("CourseParts")) {
                    String name = rs.getString("name");
                    double weight = rs.getDouble("weight");

                    Part tmpPart = new Part(name, weight);
                    if (query.contains("CoursePartResults")) {
                        String grade = rs.getString("grade");
                        int id = rs.getInt("ID");
                        int group_id = rs.getInt("group_id");

                        tmpPart.setID(id);
                        tmpPart.setGroup_id(group_id);
                        tmpPart.setGrade(grade);
                    }

                    coursePartsList.add(tmpPart);

                } else if (query.contains("Course")) {
                    String course_code = rs.getString("course_code");
                    String name = rs.getString("name");
                    int leaderID = rs.getInt("leader_id");
                    String season = rs.getString("season");
                    String exam_type = rs.getString("ExamType");
                    Course tmpCourse = new Course(course_code, name, leaderID, season, exam_type);
                    if (query.contains("cR.grade")) {
                        String grade = rs.getString("grade");
                        if (grade != null) tmpCourse.setFinalGrade(grade);
                    }

                    courseList.add(tmpCourse);

                } else if (query.contains("Students")) {
                    int studID = rs.getInt("ID");
                    String studName = rs.getString("name");

                    studentList.add(new Student(studID, studName));

                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

}