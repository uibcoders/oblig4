package no.uib.info233.v2016.sto020_can013.oblig4.application.main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root ;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../gui/GUI.fxml"));

        Controller controller = new Controller();
        controller.setMainController(controller);
        fxmlLoader.setController(controller);
        root = fxmlLoader.load();
        Scene scene = new Scene(root);
        controller.initializeGUI();

        stage.setTitle("Course Manager");
        stage.setScene(scene);
        stage.setOnCloseRequest(we -> controller.getDataSourceQuery().disconnect()); //Disconnect Database


        stage.show();
    }


    public static void main(String[] args) {

        launch(args);
    }



}
