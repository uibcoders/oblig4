package no.uib.info233.v2016.sto020_can013.oblig4.application.gui;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * SOURCE: http://blog.physalix.com/javafx2-borderpane-which-slides-in-and-out-on-command/
 * Edited by sto020 to fit project
 * <p>
 * Animates a node on and off screen to the top, right, bottom or left side.
 */
public class BorderSlideBar extends VBox {
    private double expandedSize;
    private Pos flapbarLocation;

    /**
     * Creates a sidebar panel in a BorderPane, containing an horizontal alignment
     * of the given nodes.
     * <p>
     * <pre>
     * <code>
     *  Example:
     *
     *  BorderSlideBar topFlapBar = new BorderSlideBar(
     *                  100, button, Pos.TOP_LEFT, new contentController());
     *  mainBorderPane.setTop(topFlapBar);
     * </code>
     * </pre>
     *
     * @param expandedSize The size of the panel.
     * @param location     The location of the panel (TOP_LEFT, BOTTOM_LEFT, BASELINE_RIGHT, BASELINE_LEFT).
     * @param nodes        Nodes inside the panel.
     */
    public BorderSlideBar(double expandedSize,
                          Pos location, Node... nodes) {


        setExpandedSize(expandedSize);
        setVisible(true);

        // Set location
        if (location == null) {
            flapbarLocation = Pos.TOP_CENTER; // Set default location
        }
        flapbarLocation = location;

        initPosition();

        // Add nodes in the vbox
        getChildren().addAll(nodes);
    }

    public void toggle(boolean show) {

        // Create an animation to hide the panel.
        final Animation hidePanel = new Transition() {
            {
                setCycleDuration(Duration.millis(250));
            }

            @Override
            protected void interpolate(double frac) {
                final double size = getExpandedSize() * (1.0 - frac);
                translateByPos(size);

            }
        };

        hidePanel.onFinishedProperty().set(actionEvent -> {
        });

        // Create an animation to show the panel.
        final Animation showPanel = new Transition() {
            {
                setCycleDuration(Duration.millis(300));
            }

            @Override
            protected void interpolate(double frac) {
                final double size = getExpandedSize() * frac;
                translateByPos(size);

            }
        };

        showPanel.onFinishedProperty().set(actionEvent -> {
        });


        if (!show) {

            hidePanel.play();
            //setVisible(false);


        } else {

            showPanel.play();

        }

    }


    /**
     * Initialize position orientation.
     */
    private void initPosition() {
        switch (flapbarLocation) {
            case TOP_LEFT:
                setPrefHeight(0);
                setMinHeight(0);
                break;
            case BOTTOM_LEFT:
                setPrefHeight(0);
                setMinHeight(0);
                break;
            case BASELINE_RIGHT:
                setPrefWidth(0);
                setMinWidth(0);
                break;
            case BASELINE_LEFT:
                setPrefWidth(0);
                setMinWidth(0);
                break;
        }
    }

    /**
     * Translate the VBox according to location Pos.
     *
     * @param size
     */
    private void translateByPos(double size) {
        switch (flapbarLocation) {
            case TOP_LEFT:
                setPrefHeight(size);
                setTranslateY(-getExpandedSize() + size);
                break;
            case BOTTOM_LEFT:
                setPrefHeight(size);
                break;
            case BASELINE_RIGHT:
                setPrefWidth(size);
                break;
            case BASELINE_LEFT:
                setPrefWidth(size);
                break;
        }
    }

    /**
     * @return the expandedSize
     */
    public double getExpandedSize() {
        return expandedSize;
    }

    /**
     * @param expandedSize the expandedSize to set
     */
    public void setExpandedSize(double expandedSize) {
        this.expandedSize = expandedSize;
    }

}