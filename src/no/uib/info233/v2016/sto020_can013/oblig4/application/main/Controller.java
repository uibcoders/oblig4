package no.uib.info233.v2016.sto020_can013.oblig4.application.main;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;
import no.uib.info233.v2016.sto020_can013.oblig4.application.gui.BorderSlideBar;
import no.uib.info233.v2016.sto020_can013.oblig4.course.Course;
import no.uib.info233.v2016.sto020_can013.oblig4.course.Part;
import no.uib.info233.v2016.sto020_can013.oblig4.database.DataSourceQuery;
import no.uib.info233.v2016.sto020_can013.oblig4.employee.Student;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * The controller for the GUI, handles all button presses and actions the GUI has to perform
 * Created by sto020 on 05.05.2016.
 *
 * @author sto020
 */
public class Controller implements Initializable {

    private final ObservableList<Part> tempCoursePartsList =
            FXCollections.observableArrayList(
            );
    private final ObservableList<Part> coursePartsList =
            FXCollections.observableArrayList(
            );
    private final ObservableList<Course> coursesList =
            FXCollections.observableArrayList(
            );
    private final ObservableList<Student> studentsList =
            FXCollections.observableArrayList(
            );
    private final ObservableList<Course> studentCoursesList =
            FXCollections.observableArrayList(
            );
    @FXML
    Spinner<Integer> weightSpinner;
    @FXML
    TableView<Course> tableCourses;
    @FXML
    TableView<Course> tableAvailableCourses;
    @FXML
    TableView<Part> tableStudentCourseParts;
    @FXML
    TableView<Student> tableStudents;
    @FXML
    TableView tableDetails;
    @FXML
    Button btnNewCourse, btnNewStudent;
    @FXML
    Button btnSubmit_Course, btnSubmit_Student, btnEnrollCourses;
    @FXML
    TextField txtCourse_Name, txtStudent_Name, txtSearchName;
    @FXML
    TextField txtCourse_Code;
    @FXML
    TextField txtCourse_Season;
    @FXML
    TextField txtCourse_Exam;
    @FXML
    Button btnCoursePart_Add;
    @FXML
    TextField txtCoursePart_Name;
    @FXML
    TableView<Part> tableCourseParts;
    @FXML
    TabPane tabHolder;
    @FXML
    Tab tabCourses, tabStudents;
    @FXML
    BorderPane borderPane;
    @FXML
    Label labelDetails, labelConnection;

    private Stage stage;
    private BorderSlideBar rightTableView;
    private int spinnerMax = 100;

    private Controller mainController = null;
    private int selectedStudentID = 0;
    private final MenuItem addMenuItem = new MenuItem("Add Course");
    private SpinnerValueFactory<Integer> svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(20, spinnerMax, 20, 10);

    private DataSourceQuery dataSourceQuery;

    public DataSourceQuery getDataSourceQuery() {
        return dataSourceQuery;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (location.getFile().contains("CourseDialog.fxml")) {
            initializeCourseDialog();

        }
    }


    /**
     * Initializes the main GUI window.
     * Adds all necessary lists and listeners
     * Gets data from database
     */
    public void initializeGUI() {
        dataSourceQuery = new DataSourceQuery();
        labelConnection.setText(dataSourceQuery.connect());
        tableStudentCourseParts = new TableView<>();
        tableStudentCourseParts.setPrefHeight(tableDetails.getPrefHeight());
        setCoursePartsTableColumns(tableStudentCourseParts);

        rightTableView = new BorderSlideBar(370, Pos.BASELINE_RIGHT, tableStudentCourseParts);

        setMainTableColumns();

        tableStudents.setItems(studentsList);
        tableCourses.setItems(coursesList);

        if (labelConnection.getText().equals("Connected")) {
            getCourses();
            getStudents();
            setDetailsTableColumnsCourse();
            addListeners();
        } else {

        }
    }

    /**
     * Initializes the Course dialog for adding new courses to the database
     * Sets the appropriate columns for the tables in the dialog.
     */
    public void initializeCourseDialog() {
        weightSpinner.setValueFactory(svf);
        setCoursePartsTableColumns(tableCourseParts);
    }

    /**
     * Initializes the dialog for adding new courses to the student, only shows the ones not already enrolled
     * Sets the appropriate columns for the tables in the dialog.
     */
    public void initializeAvailableCourses() {
        setCourseTableColumns(tableAvailableCourses);
        getCourses();
        getSelectedStudentCourses();
        removeEnrolledCoursesFromList();
    }

    /**
     * Adds listeners to the different TableView's
     * When a table item is selected the listeners will perform their specified action
     */
    private void addListeners() {
        tabHolder.getSelectionModel().selectedItemProperty().addListener(
                (ov, t, t1) -> {
                    if (t1.getId().equals(tabStudents.getId())) {
                        setDetailsTableColumnsStudent();
                        getStudents();
                        rightTableView.toggle(true);//Make the Part table come into view
                        tableStudents.getSelectionModel().select(0); //select first Student on list

                    } else if (t1.getId().equals(tabCourses.getId())) {
                        setDetailsTableColumnsCourse();
                        getCourses();
                        rightTableView.toggle(false); //Make the Part table go away

                    }
                }
        );
        txtSearchName.textProperty().addListener((observable, oldValue, newValue) -> {
            searchStudent();
        });

        tableCourses.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            getSelectedCourseParts();
        });

        tableStudents.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            getSelectedStudentCourses();

            tableDetails.getSelectionModel().select(0); //select first Course on list


        });

        tableDetails.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                getSelectedCoursePartsWithGrades();
            } catch (ClassCastException c) {
                //Just the wrong table
            }
        });

        btnNewCourse.setOnAction(event -> newCourse());
        btnNewStudent.setOnAction(event -> newStudent());


    }

    /**
     * Sets the currently used student ID in the Controller
     *
     * @param student_id The student ID to set
     */
    public void setSelectedStudentID(int student_id) {
        this.selectedStudentID = student_id;
    }

    /**
     * Sets the Main controller, meaning the one from the first windows eg. the GUI
     *
     * @param controller The main controller, not a window controller
     */
    public void setMainController(Controller controller) {
        this.mainController = controller;
    }

    /**
     * Iterates through studentCoursesList and coursesList to find common course_codes.
     * If they are the same, the course will be removed from courseList to prevent adding them again.
     */
    private void removeEnrolledCoursesFromList() {
        Iterator<Course> sIter = studentCoursesList.iterator();


        while (sIter.hasNext()) { //Iterates studentCoursesList
            Course c = sIter.next();
            Iterator<Course> cIter = coursesList.iterator();

            while (cIter.hasNext()) { //Iterates coursesList
                Course c2 = cIter.next();
                if (c.getCourse_code().equals(c2.getCourse_code())) {
                    cIter.remove(); //Removes item from coursesList if matching code found in studentCoursesList
                }
            }
        }
    }


    /**
     * Sets the headers for Course parts to a TableView
     *
     * @param table The table to set te parts headers to
     */
    private void setCoursePartsTableColumns(TableView<Part> table) {

        //When CourseDialog is opened
        table.setEditable(true);
        TableColumn<Part, String> nameCol = new TableColumn("Part name");
        nameCol.prefWidthProperty().set(150);
        nameCol.setCellValueFactory(new PropertyValueFactory("name"));//Gets 'name' from Part

        TableColumn<Part, Double> weightCol = new TableColumn("Weight(%)");
        weightCol.prefWidthProperty().set(100);
        weightCol.setCellValueFactory(
                new PropertyValueFactory("weight")//Gets 'weight' from Part
        );

        TableColumn gradeCol = new TableColumn("Grade");
        gradeCol.prefWidthProperty().set(80);
        gradeCol.setCellValueFactory(
                new PropertyValueFactory<Part, String>("grade")//Gets 'grade' from Part
        );


        gradeCol.setCellFactory(TextFieldTableCell.forTableColumn());

        gradeCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent t) {
                        Part tmp = (((Part) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()))
                        );

                        Course selectedCourse = ((Course) tableDetails.getSelectionModel().getSelectedItem());
                        String selectedCourseCode = selectedCourse.getCourse_code();

                        if (tmp.getGrade() == null) {
                            //Setting Part Grade
                            tmp.setGrade(t.getNewValue().toString().toUpperCase());
                            if(tmp.getGroup_id() == 0) {
                                mainController.getDataSourceQuery().setPartGrade(selectedCourseCode, tmp.getGrade(), tmp.getID(), selectedStudentID);
                            }
                            else
                            {
                                mainController.getDataSourceQuery().setPartGradeToGroup(selectedCourseCode, tmp.getGrade(), tmp.getID(), selectedStudentID, tmp.getGroup_id());

                            }
                        } else {
                            //Updating Part Grade
                            tmp.setGrade(t.getNewValue().toString().toUpperCase());
                            mainController.getDataSourceQuery().updatePartGrade(tmp.getGrade(), tmp.getID(), selectedStudentID,tmp.getGroup_id());

                        }

                        selectedCourse.updateCoursePartGrade(tmp);

                        if (selectedCourse.getFinalGrade() == null) {
                            String finalGrade = selectedCourse.calculateFinalGrade();
                            if (finalGrade != null) {
                                mainController.getDataSourceQuery().setCourseGrade(selectedCourseCode, selectedCourse.calculateFinalGrade(), selectedStudentID);
                            }
                        } else {
                            mainController.getDataSourceQuery().updateCourseGrade(selectedCourseCode, selectedCourse.calculateFinalGrade(), selectedStudentID);
                        }

                        tableDetails.refresh(); //Refresh the data in the table to show grade
                    }
                });

        TableColumn groupCol = new TableColumn("Group");
        groupCol.prefWidthProperty().set(80);
        groupCol.setCellValueFactory(
                new PropertyValueFactory<Part, Integer>("group_id")//Gets 'group_id' from Part
        );

        groupCol.setCellFactory(
                TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        groupCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent t) {
                        Part tmp = (((Part) t.getTableView().getItems().get(
                                t.getTablePosition().getRow()))
                        );

                        Course selectedCourse = ((Course) tableDetails.getSelectionModel().getSelectedItem());
                        String selectedCourseCode = selectedCourse.getCourse_code();

                        int new_group_id = (Integer)t.getNewValue();
                        //Updating Part Group id
                        mainController.getDataSourceQuery().updateStudentGroupID(selectedCourseCode, selectedStudentID, tmp.getGroup_id(), new_group_id);
                        tmp.setGroup_id(new_group_id);

                        tableDetails.refresh(); //Refresh the data in the table to show grade
                    }
                });


        table.setItems(tempCoursePartsList);
        if (table.getId() != null) {
            table.getColumns().addAll(nameCol, weightCol);
            btnSubmit_Course.setDisable(true); //Disable button
        } else {
            table.getColumns().addAll(nameCol, weightCol, gradeCol, groupCol);
        }
    }

    /**
     * Sets the Columns for the main TableViews eg. Students and Courses
     */

    private void setMainTableColumns() {
        setCourseTableColumns(tableCourses);

        //Adding students TableView columns
        tableStudents.setEditable(true);
        TableColumn idColStudents = new TableColumn("ID");
        idColStudents.prefWidthProperty().set(100);
        idColStudents.setCellValueFactory
                (new PropertyValueFactory<Student, Integer>("ID"));//Gets 'ID' from Student

        TableColumn<Student, String> nameColStudents = new TableColumn("FirstName");
        nameColStudents.prefWidthProperty().set(350);
        nameColStudents.setCellValueFactory(
                new PropertyValueFactory<>("firstName")//Gets 'firstName' from Student
        );


        tableStudents.setItems(studentsList);
        tableStudents.getColumns().addAll(idColStudents, nameColStudents);

    }

    /**
     * Sets the Columns template for Courses
     *
     * @param table the TableView to set the columns
     */
    private void setCourseTableColumns(TableView table) {
        //Adding Courses TableView columns

        table.setEditable(true);
        TableColumn codeColCourse = new TableColumn("Course Code");
        codeColCourse.prefWidthProperty().set(100);
        codeColCourse.setCellValueFactory
                (new PropertyValueFactory<Course, String>("course_code"));//Gets 'course_code' from Course

        TableColumn<Course, String> nameColCourse = new TableColumn("Name");
        nameColCourse.prefWidthProperty().set(200);
        nameColCourse.setCellValueFactory(
                new PropertyValueFactory("name")//Gets 'name' from Course
        );


        table.setItems(coursesList);

        if (table == tableAvailableCourses) {

            TableColumn checkColCourse = new TableColumn("Enroll");
            checkColCourse.prefWidthProperty().set(50);

            checkColCourse.setCellValueFactory(new PropertyValueFactory("enroll"));//Gets 'enroll' from Course
            checkColCourse.setCellFactory(CheckBoxTableCell.forTableColumn(checkColCourse));

            table.getColumns().addAll(codeColCourse, nameColCourse, checkColCourse);

        } else if (table == tableCourses) {

            table.getColumns().addAll(codeColCourse, nameColCourse);


        }

    }


    /**
     * Sets the Columns for the Details TableView.  --> The table which shows
     * the extra information about the Courses
     */
    private void setDetailsTableColumnsCourse() {
        tableDetails.getColumns().clear(); //clear the table
        tableDetails.setEditable(true);

        TableColumn nameCol = new TableColumn("Name");
        nameCol.prefWidthProperty().set(150);
        nameCol.setCellValueFactory
                (new PropertyValueFactory<Part, String>("name"));//Gets 'name' from Part

        TableColumn<Part, String> weightCol = new TableColumn<>("Weight(%)");
        weightCol.prefWidthProperty().set(70);
        weightCol.setCellValueFactory(
                new PropertyValueFactory<>("weight")//Gets 'weight' from Part
        );


        tableDetails.setItems(coursePartsList);
        tableDetails.getColumns().addAll(nameCol, weightCol);
        tableDetails.setContextMenu(null);
        labelDetails.setText("Course Parts");
    }


    /**
     * Sets the Columns for the Details TableView.  --> The table which shows
     * the extra information about the Students
     */
    private void setDetailsTableColumnsStudent() {
        tableDetails.getColumns().clear();//clear the table

        tableDetails.setEditable(true);

        TableColumn codeColCourse = new TableColumn("Course Code");//TableColumn for Course Code
        codeColCourse.prefWidthProperty().set(80);
        codeColCourse.setCellValueFactory(
                new PropertyValueFactory<Course, String>("course_code"));//Gets 'course_code' from Course

        TableColumn<Course, String> nameColCourse = new TableColumn<>("Name");//TableColumn for Course name
        nameColCourse.prefWidthProperty().set(190);
        nameColCourse.setCellValueFactory(
                new PropertyValueFactory<>("name") //Gets 'name' from Course
        );

        TableColumn<Course, String> gradeColCourse = new TableColumn<>("Grade"); //TableColumn for Grade
        gradeColCourse.prefWidthProperty().set(60);
        gradeColCourse.setCellValueFactory(
                new PropertyValueFactory<>("finalGrade") //Gets 'finalGrade' from Course
        );


        tableDetails.setItems(studentCoursesList); //Set the tableDetails item list

        tableDetails.getColumns().addAll(codeColCourse, nameColCourse, gradeColCourse); //Add Columns to tableDetails


        ContextMenu tableContextMenu = new ContextMenu();
        tableContextMenu.setOnShowing(event -> { //When the ContextMenu is shown execute event
            Student student = tableStudents.getSelectionModel().getSelectedItem();
            if (student != null) {
                addMenuItem.disableProperty().set(false);

                addMenuItem.setText("Add Course to " + student.getFirstName());
            } else {

                addMenuItem.disableProperty().set(true);
                addMenuItem.setText("Select A Student");

            }
        });

        addMenuItem.setOnAction(event -> enrollCourses()); //When the add menu button is clicked, open windows to enroll

        tableContextMenu.getItems().addAll(addMenuItem); //Add item to ContextMenu

        tableDetails.setContextMenu(tableContextMenu); //Add ContextMenu to tableDetails

        borderPane.setRight(rightTableView); //create table to slide in from the right when toggled

        labelDetails.setText("Student Courses");
    }


    /**
     * Gets the Courses the Student has enrolled
     */
    public void getSelectedStudentCourses() {
        studentCoursesList.clear();//clear the list

        if (tableStudents != null) {
            if (tableStudents.getSelectionModel().getSelectedItem() != null) {
                selectedStudentID = tableStudents.getSelectionModel().getSelectedItem().getID();
            }
            if (!tableStudents.getItems().isEmpty()) {
                labelDetails.setText(tableStudents.getSelectionModel().getSelectedItem().getFirstName());
            }
        }
        studentCoursesList.addAll(mainController.getDataSourceQuery().getCoursesByStudentID(selectedStudentID));


    }

    /**
     * Gets the Course parts for the selected Course
     */
    public void getSelectedCourseParts() {
        coursePartsList.clear();//clear the list

        if (!tableCourses.getItems().isEmpty()) {
            Course selectedCourse = tableCourses.getSelectionModel().getSelectedItem();

            System.out.println("Querying Course Parts for " + selectedCourse.getCourse_code());

            coursePartsList.addAll(mainController.getDataSourceQuery().getCourseParts(selectedCourse.getCourse_code()));


        }
    }

    /**
     * Gets the Course parts for the selected Course
     */
    public void getSelectedCoursePartsWithGrades() {
        tempCoursePartsList.clear();//clear the list

        if (tableDetails.getItems() != null) {
            if (!tableDetails.getItems().isEmpty() && tableDetails.getSelectionModel().getSelectedItem() != null) {
                Course selectedCourse = ((Course) tableDetails.getSelectionModel().getSelectedItem());


                tempCoursePartsList.addAll(mainController.getDataSourceQuery().getCoursePartsWithGrades(selectedCourse.getCourse_code(), selectedStudentID));
                tempCoursePartsList.forEach(selectedCourse::addCoursePart);


            }
        }

    }


    /**
     * Searches for a Student name based on input from txtSearchName
     */
    public void searchStudent() {
        studentsList.clear();
        String name = txtSearchName.getText();
        if (name.isEmpty()) {
            getStudents();
            return;
        }

        System.out.println("Querying Student " + name);

        studentsList.addAll(mainController.getDataSourceQuery().searchStudent(name));

    }

    /**
     * Gets all Students from the DataBase.
     * Adds the to the Students Table
     */

    public void getStudents() {
        studentsList.clear();
        System.out.println("Querying Students");

        studentsList.addAll(mainController.getDataSourceQuery().getStudents());


    }

    /**
     * Gets all Courses from the DataBase.
     * Adds the to the Courses Table
     */
    public void getCourses() {
        coursesList.clear();

        System.out.println("Querying Courses");

        coursesList.addAll(mainController.getDataSourceQuery().getAllCourses());


    }


    /********************************
     * Code For the Popup Windows
     ********************************/


    /**
     * Creates a Popup to Enroll student in Courses
     */
    public void enrollCourses() {
        createDialog("../dialogs/AvailableCoursesDialog.fxml", "Available Courses for: " + selectedStudentID);
    }

    /**
     * Creates a Popup to add a new Course
     */
    public void newCourse() {

        createDialog("../dialogs/CourseDialog.fxml", "New Course");

    }

    /**
     * Creates a Popup to add a new Students
     */
    public void newStudent() {
        createDialog("../dialogs/StudentDialog.fxml", "New Student");

    }

    /**
     * Creates a Popup Dialog from the given fxml file
     *
     * @param fxmlFile - The fxml file with the Layout of the Popup
     */
    private void createDialog(String fxmlFile, String title) {
        VBox root;
        stage = new Stage();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlFile));
            Controller controller = new Controller();
            controller.setSelectedStudentID(this.selectedStudentID); //Pass studentID to new controller
            controller.setMainController(this.mainController);
            fxmlLoader.setController(controller);
            root = fxmlLoader.load();

            if (fxmlFile.equals("../dialogs/AvailableCoursesDialog.fxml")) controller.initializeAvailableCourses();
            if (fxmlFile.equals("../dialogs/CourseDialog.fxml")) controller.initializeCourseDialog();

            stage.setScene(new Scene(root));
            stage.setTitle(title);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(btnNewCourse.getScene().getWindow());
            stage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Adds a new Course Part to the Course popups TableView.
     * Checks if Weight is correct and changes the Spinner to hold only the remanding Weight
     */
    public void addNewCoursePart() {

        if (txtCoursePart_Name.getText().isEmpty()) {
            return;
        }
        int weight = (weightSpinner.getValueFactory().getValue()); //Gets value from Spinnner
        int remaining = spinnerMax - weight;

        if (remaining < 20) { //Is the new weight smaller then 20
            if (remaining == 0) { //Is the new weight 0
                btnCoursePart_Add.setDisable(true); //Disable button for adding more
                weightSpinner.setDisable(true);
                txtCoursePart_Name.setDisable(true);
                btnSubmit_Course.setDisable(false); //Enable button


            } else {
                return; //cannot be, abort
            }

        }

        Part temp = new Part(txtCoursePart_Name.getText(), weight); //Create new Part with Params
        temp.setID(tempCoursePartsList.size() + 1); //temporary id so the map doesn't get confused
        if (remaining >= 20) {
            spinnerMax = remaining;
            SpinnerValueFactory<Integer> newSvf = new SpinnerValueFactory.IntegerSpinnerValueFactory(20, spinnerMax, 20, 10);
            weightSpinner.setValueFactory(newSvf);
        }

        tempCoursePartsList.add(temp);

    }


    /**
     * Adds the Course Parts to the Course.
     * Submits the newly created course to the database.
     * Closes the popup
     */
    public void submitCourse() {

        Course tmpCourse = new Course(txtCourse_Code.getText().toUpperCase(), txtCourse_Name.getText(), 0, txtCourse_Season.getText(), txtCourse_Exam.getText());

        this.tempCoursePartsList.forEach(tmpCourse::addCoursePart);

        mainController.getDataSourceQuery().addCourse(tmpCourse);
        this.mainController.getCourses();
        stage = (Stage) btnSubmit_Course.getScene().getWindow();

        stage.close();


    }

    /**
     * Submits a student to the database
     * Updates the main student list
     * Closes the dialog
     * Only call from StudentDialog
     */
    public void submitStudent() {

        Student tmpStudent = new Student(0, txtStudent_Name.getText());
        mainController.getDataSourceQuery().addStudent(tmpStudent);
        this.mainController.getStudents();
        stage = (Stage) btnSubmit_Student.getScene().getWindow();

        stage.close();


    }

    /**
     * Enrolls the student to all the courses found in the coursesList,
     * which is generated by checking courses from a list in
     * AvailableCourses dialog
     * Then closes the dialog
     * Only call from AvailableCourses dialog
     */
    public void enrollStudent() {
        coursesList.stream().filter(course -> course.enrollProperty().getValue()).forEach(course -> {
            mainController.getDataSourceQuery().enrollStudentToCourse(course.getCourse_code(), selectedStudentID,0);
            course.enrollProperty().setValue(false); //Reset
        });

        mainController.getSelectedStudentCourses();

        stage = (Stage) btnEnrollCourses.getScene().getWindow();

        stage.close();
    }


}
